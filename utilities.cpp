#include <iostream>
#include <vector>

#include "utilities.hpp"

using namespace std;

void print_vector(const vector<int> &v) {
  cout << "[";
  string c = "";
  for (int i=0; i<v.size();i++){
    cout << c << v[i];
    c = ",";
  }
  cout << "]";
}

void sort_vectors(vector<int> &v, vector<int> &v_aux1) {
    _quick_sort(v, v_aux1, 0, v.size() - 1);
}

void array_init_int(int *array, int size, int value){
  for(int i = 0; i < size; i++){
    array[i] = value;
  }
}
void array_init_float(float *array, int size, float value){
  for(int i = 0; i < size; i++){
    array[i] = value;
  }
}

int * array_copy(int size, int * arr) {
  int i;
  int *new_arr = new int[size];
  for (i = 0; i < size; i++) {
    new_arr[i] = arr[i];
  }
  return new_arr;
}

float * array_copy(int size, float * arr) {
  int i;
  float *new_arr = new float[size];
  for (i = 0; i < size; i++) {
    new_arr[i] = arr[i];
  }
  return new_arr;
}

void print_array(int * array, int size) {
  cout << "[";
  string c = "";
  for (int i=0; i<size;i++){
    cout << c << array[i];
    c = ",";
  }
  cout << "]";
}

void print_array(float * array, int size) {
  cout << "[";
  string c = "";
  for (int i=0; i<size;i++){
    cout << c << array[i];
    c = ",";
  }
  cout << "]";
}

void _quick_sort(vector<int> &v, vector<int> &v_aux1, int l, int h) {
    // Create an auxiliary stack
    int stack[ h - l + 1 ];

    // initialize top of stack
    int top = -1;

    // push initial values of l and h to stack
    stack[ ++top ] = l;
    stack[ ++top ] = h;

    // Keep popping from stack while is not empty
    while ( top >= 0 ) {
        // Pop h and l
        h = stack[ top-- ];
        l = stack[ top-- ];

        // Set pivot element at its correct position
        // in sorted array
        int p = partition( v, v_aux1, l, h );

        // If there are elements on left side of pivot,
        // then push left side to stack
        if ( p-1 > l ) {
            stack[ ++top ] = l;
            stack[ ++top ] = p - 1;
        }

        // If there are elements on right side of pivot,
        // then push right side to stack
        if ( p+1 < h ) {
            stack[ ++top ] = p + 1;
            stack[ ++top ] = h;
        }
    }
}

int partition (vector<int> &v, vector<int> &v_aux1, int l, int h) {
    int x = v[h];
    int i = (l - 1);

    for (int j = l; j <= h- 1; j++) {
        if (v[j] <= x) {
            i++;
            swap (i, j, v);
            swap (i, j, v_aux1);
        }
    }
    swap (i+1, h, v);
    swap (i+1, h, v_aux1);
    return (i + 1);
}

void swap ( int i, int j, vector<int> &v) {
    int t = v.at(i);
    v.at(i) = v.at(j);
    v.at(j) = t;
}
