#ifndef MASTERINFORMATION_HPP
#define MASTERINFORMATION_HPP

#include <vector>
#include <string>

#include "TransferClasses.hpp"
#include "CommonInformation.hpp"

class MasterInformation {
  // Problem variables
  int * best_assignation;
  CommonInformation commonInformation;
  int num_workers;
  int accumulated_weight;
  int accumulated_value;
  int * indexs;
  // Current Knapsack variables
  int current_value;
  int current_weight;
  int current_knapsack;
  int min_weight;
  int max_weight;
  int min_objects;
  int max_objects;
  std::vector<ToWorkInfo *> work_to_send;
  int nrestarts;
  int niterations;
  std::vector<int> knapsacks_weights;
  std::vector<int> knapsacks_values;
public:
  MasterInformation(int n_workers, CommonInformation c);
  int get_min_weight();
  int get_max_weight();
  int get_min_objects();
  int get_max_objects();
  int get_num_workers();
  bool is_finished();
  int add_new_knapsack();
  int * create_indexs_array();
  void create_work();
  std::vector<ToWorkInfo *> get_work_to_send();
  int get_best_proposal(std::vector<Proposal *> &proposals);
  bool is_proposal_good_enough(Proposal &proposal);
  void create_work_from_proposal(Proposal &proposal);
  void generate_finish_work();
  int * get_assignation();
  int get_current_knapsack();
  void write_results_to_file(std::string path);
private:
  void calculate_min_objects();
  void calculate_max_objects();
  float * create_probabilities_array(int preferent_zone, float max_prob);
  ToWorkInfo * generate_to_work_struct(int iterations, int restarts, float * probs);
  void clear_work_to_send_vector();
};

#endif
