#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "MasterInformation.hpp"
#include "utilities.hpp"
#include "TransferClasses.hpp"

using namespace std;

/********************
 *  PUBLIC METHODS  *
 ********************/

MasterInformation::MasterInformation(int n_workers, CommonInformation c) {
  // Problem variables init
  commonInformation = c; // CommonInformation
  best_assignation = new int[commonInformation.get_total_items()];
  // No object has been assigned yet
  array_init_int(best_assignation, commonInformation.get_total_items(), 0);
  num_workers = n_workers;
  indexs = MasterInformation::create_indexs_array();
  current_value = 0; // Empty knapsack
  current_weight = 0; // Empty knapsack
  current_knapsack = 0; // No knapsack. Not started yet
  accumulated_weight = 0; // We have no accumulated weight
  accumulated_value = 0; // We have no accumulated value
  niterations = commonInformation.get_knapsack_capacity();
  nrestarts = (niterations * 2) / num_workers;
}

int MasterInformation::get_min_weight() {
  return min_weight;
}

int MasterInformation::get_max_weight(){
  return max_weight;
}

int MasterInformation::get_min_objects(){
  return min_objects;
}

int MasterInformation::get_max_objects() {
  return max_objects;
}

int MasterInformation::get_num_workers() {
  return num_workers;
}

bool MasterInformation::is_finished() {
  return (commonInformation.get_total_weight() - accumulated_weight) <=
            commonInformation.get_knapsack_capacity();
}

int MasterInformation::add_new_knapsack() {
  // Update min and max objects
  MasterInformation::calculate_min_objects();
  MasterInformation::calculate_max_objects();
  // Increment number of knapsack
  current_knapsack++;
  // Set current_weight to 0
  current_weight = 0;
  // Set current_value to 0
  current_value = 0;
  // Return current_knapsack
  return current_knapsack;
}

int * MasterInformation::create_indexs_array() {
  int * indexs = new int[commonInformation.get_num_zones()];
  int i;
  int percentage = 10;

  for (i = 0; i < commonInformation.get_num_zones(); i++) {
    indexs[i] = (int) (commonInformation.get_total_items() * percentage / 100);
    percentage += 10;
  }
  return indexs;
}

void MasterInformation::create_work() {
  int i;
  float * probabilities;
  int zone;
  MasterInformation::clear_work_to_send_vector();

  for (i = 0; i < num_workers; i++) {
    // Common information in all workers
    ToWorkInfo * s;
    zone = i % commonInformation.get_num_zones();
    probabilities = MasterInformation::create_probabilities_array(zone, 50);
    s = MasterInformation::generate_to_work_struct(niterations,
                                                   nrestarts, probabilities);
    work_to_send.push_back(s);
  }
}

vector<ToWorkInfo *> MasterInformation::get_work_to_send() {
  return work_to_send;
}

int MasterInformation::get_best_proposal(vector<Proposal *> &proposals) {
  int max_weight = -1;
  int best_index = -1;
  for (int i = 0; i < proposals.size(); i++) {
    if (proposals[i]->best_weight > max_weight) {
      max_weight = proposals[i]->best_weight;
      best_index = i;
    } else if (proposals[i]->best_weight == max_weight &&
               proposals[i]->best_value > proposals[best_index]->best_value) {
      best_index = i;
    }
  }

  return best_index;
}

bool MasterInformation::is_proposal_good_enough(Proposal &proposal) {
  bool has_improved = false;
  if(proposal.best_weight == current_weight){
    current_weight = proposal.best_weight;
    current_value = proposal.best_value;
    knapsacks_weights.push_back(current_weight);
    knapsacks_values.push_back(current_value);
    accumulated_weight += current_weight;
    accumulated_value += current_value;
    if (best_assignation != nullptr) {
      delete[]best_assignation;
      best_assignation = nullptr;
    }
    best_assignation = array_copy(commonInformation.get_total_items(), proposal.best_solution);
    has_improved = true;
  }

  return has_improved;
}

void MasterInformation::create_work_from_proposal(Proposal &proposal) {
  if (best_assignation != nullptr) {
    delete[]best_assignation;
    best_assignation = nullptr;
  }
  best_assignation = array_copy(commonInformation.get_total_items(), proposal.best_solution);
  current_weight = proposal.best_weight;
  current_value = proposal.best_value;
  MasterInformation::clear_work_to_send_vector();

  for (int i = 0; i < num_workers; i++) {
    // Common information in all workers
    ToWorkInfo * s;
    s = MasterInformation::generate_to_work_struct(niterations, nrestarts,
                                                   proposal.probabilities);
    work_to_send.push_back(s);
  }
}

void MasterInformation::generate_finish_work() {
  current_weight = -1;  // Finish flag
  float * probs = new float[commonInformation.get_num_zones()];
  array_init_float(probs, commonInformation.get_num_zones(), 0.0f);
  MasterInformation::clear_work_to_send_vector();

  for (int i = 0; i < num_workers; i++) {
    // Common information in all workers
    ToWorkInfo * s;
    s = MasterInformation::generate_to_work_struct(niterations, nrestarts, probs);
    work_to_send.push_back(s);
  }
}

int * MasterInformation::get_assignation() {
  return best_assignation;
}

int MasterInformation::get_current_knapsack() {
  return current_knapsack;
}

void MasterInformation::write_results_to_file(string path) {
  ofstream outputfile;
  outputfile.open(path);
  outputfile << current_knapsack + 1 << endl;
  for (int i = 0; i < knapsacks_weights.size(); i++) {
    outputfile << knapsacks_values[i] << ",";
    outputfile << knapsacks_weights[i] << endl;
  }
  // Last knapsack
  outputfile << commonInformation.get_total_value() - accumulated_value << ",";
  outputfile << commonInformation.get_total_weight() - accumulated_weight << endl;
  outputfile.close();
}

/********************
 *  PRIVATE METHODS *
 ********************/

void MasterInformation::calculate_min_objects(){
  for(int i = (commonInformation.get_total_items() -1); i >= 0; i--){
    if(best_assignation[i] == 0){
      max_weight = commonInformation.get_weight_of_index(i);
      break;
    }
  }
  min_objects = commonInformation.get_knapsack_capacity() / max_weight;
}

void MasterInformation::calculate_max_objects() {
  for (int i = 0; i < commonInformation.get_total_items(); i++) {
    if (best_assignation[i] == 0) { // Unassigned item
      min_weight = commonInformation.get_weight_of_index(i);
      break;
    }
  }
  max_objects = commonInformation.get_knapsack_capacity() / min_weight;
}

float * MasterInformation::create_probabilities_array(int preferent_zone,
                                                      float max_prob) {
  float * probabilities = new float[commonInformation.get_num_zones()];
  float standard_prob = (100 - max_prob) / (commonInformation.get_num_zones() - 1);
  int i;
  float accumulated_prob = 0;

  for (i = 0; i < commonInformation.get_num_zones(); i++) {
    if (i == preferent_zone) {
      accumulated_prob += max_prob;
      probabilities[i] = accumulated_prob;
    } else {
      accumulated_prob += standard_prob;
      probabilities[i] = accumulated_prob;
    }
  }
  return probabilities;
}

ToWorkInfo * MasterInformation::generate_to_work_struct(int iterations,
                                                      int restarts, float * probs) {
 ToWorkInfo * s = new ToWorkInfo();
 s->assignments = array_copy(commonInformation.get_total_items(), best_assignation);
 s->num_items = commonInformation.get_total_items();
 s->current_value = current_value;
 s->current_weight = current_weight;
 s->current_knapsack = current_knapsack;
 s->max_iterations = iterations;
 s->max_tries = restarts;
 s->min_weight = min_weight;
 // Creation of indexs (common for every worker)
 s->indexs = array_copy(commonInformation.get_num_zones(), indexs);
 s->num_probabilities = commonInformation.get_num_zones();
 // Probability generation (different for every worker)
 s->probabilities = array_copy(commonInformation.get_num_zones(), probs);
 return s;
}

void MasterInformation::clear_work_to_send_vector() {
  for (int i = 0; i < work_to_send.size(); i++) {
    if (work_to_send[i] != nullptr) {
      delete work_to_send[i];
      work_to_send[i] = nullptr;
    }
  }
  work_to_send.clear();
}
