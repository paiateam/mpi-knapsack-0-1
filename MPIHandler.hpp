#ifndef MPIHANDLER_HPP
#define MPIHANDLER_HPP

#include <vector>
#include "TransferClasses.hpp"

class MPIHandler {
  static int to_work_struct_size;
  static int proposal_struct_size;
public:
  static void init(int num_items, int num_probabilities);
  static void pack_to_work_info(ToWorkInfo &info, char *buffer);
  static void pack_proposal_info(Proposal &info, char *buffer);
  static ToWorkInfo * unpack_to_work_info(char *buffer);
  static Proposal * unpack_proposal_info(char *buffer);
  static int get_to_work_struct_size();
  static int get_proposal_struct_size();
  static void scatter_to_work_data(std::vector<ToWorkInfo *> data);
  static ToWorkInfo * receive_master_data();
  static void send_data_to_master(Proposal * data);
  static void gather_data_from_workers(std::vector<Proposal *> &proposals, int size);
private:
  static int calculate_to_work_struct_size(int num_items, int num_probabilities);
  static int calculate_proposal_struct_size(int num_items, int num_probabilities);
  MPIHandler();
};

#endif
