#include <iostream>
#include <vector>
#include "mpi.h"

#include "CommonInformation.hpp"
#include "MasterInformation.hpp"
#include "WorkerInformation.hpp"
#include "TransferClasses.hpp"
#include "MPIHandler.hpp"
#include "utilities.hpp"

using namespace std;

int main(int argc, char **argv) {
  // MPI control variables
  int rank, size;

  // Start of MPI
  MPI_Init(&argc, &argv);

  // MPI information
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // Argument parsing
  if (argc != 2 && rank == 0) {
    cout << "USAGE: " << argv[0] << " <InstanceFile>" << endl;
    exit(EXIT_FAILURE);
  }

  // Common Information reading
  CommonInformation commonInformation;
  commonInformation.set_rank(rank);
  commonInformation.read_file(argv[1]);

  MPIHandler::init(commonInformation.get_total_items(),
                   commonInformation.get_num_zones());

  if (rank == 0) {
    // MASTER! MASTER! \m/
    double start_time, end_time;
    start_time = MPI_Wtime();
    cout << "[MASTER] Starting Knapsack solver with " << size -1 << " workers" << endl;
    cout << "[PROBLEM CONFIGURATION]" << endl;
    commonInformation.print_info();
    cout << "---------------------------------------" << endl;
    MasterInformation masterInformation = MasterInformation(size-1, commonInformation);
    vector<Proposal *> proposals;
    int proposal_index;
    int current_knapsack;
    // Main loop. While there is some object unassigned
    while (!masterInformation.is_finished()) {
      // ---- Knapsack init ----
      current_knapsack = masterInformation.add_new_knapsack();
      if (current_knapsack % 200 == 0) {
        cout << "[MASTER] Working in knapsack " << current_knapsack << endl;
      }
      // Create n_workers ToWorkInfo instances
      masterInformation.create_work();
      // Knapsack loop. While knapsack is not good enough
      // or a maximum iterations is reached
      while (true) {
        // Send work to workers
        vector<ToWorkInfo *> work = masterInformation.get_work_to_send();
        MPIHandler::scatter_to_work_data(work);
        // Receive results from workers
        MPIHandler::gather_data_from_workers(proposals, masterInformation.get_num_workers());
        proposal_index = masterInformation.get_best_proposal(proposals);
        if (!masterInformation.is_proposal_good_enough(*proposals[proposal_index])) {
          masterInformation.create_work_from_proposal(*proposals[proposal_index]);
        } else {
          break;
        }
      }
    }

    // Send to workers the final message (GG & WP)
    masterInformation.generate_finish_work();
    MPIHandler::scatter_to_work_data(masterInformation.get_work_to_send());
    current_knapsack++; // The zero knapsack will be the last knapsack
    end_time = MPI_Wtime();

    // Write results to file
    string inputfile(argv[1]);
    string outputfile = inputfile + "-output";
    masterInformation.write_results_to_file(outputfile);

    cout << "*****************************" << endl;
    cout << "* [MASTER] End of execution *" << endl;
    cout << "*****************************" << endl;
    cout << "Num Knapsacks: " << current_knapsack << endl;
    cout << "Time elapsed (in seconds): " << end_time - start_time << endl;
    cout << "Results written in " << outputfile << " file" << endl;

  } else {
    int next_item_index = -1;
    WorkerInformation workerInformation = WorkerInformation(commonInformation, rank);
    Proposal * proposal = NULL;
    ToWorkInfo * info = NULL;

    // While master wants the workers alive
    while(true) {
      // Get information from master
      info = MPIHandler::receive_master_data();
      workerInformation.read_new_work(info);
      if (info->current_weight == -1) {
        // Finish flag
        break;
      }
      // While max_tries are not reached
      for(int max_tries = 0; max_tries < workerInformation.get_max_tries(); max_tries++){
        workerInformation.search_init();
        // While max_fips are not reached or can not find a better solution
        for(int max_it = 0; max_it < workerInformation.get_max_iterations(); max_it++){
          // Greedy and incremental local search
          next_item_index = workerInformation.get_next_item();
          //Add item
          if(next_item_index == -1)
          continue;
          if(!workerInformation.add_item_attempt(next_item_index)){
            break;
          }
        }
        workerInformation.search_finish();
      }
      // Send to master the best solution found
      proposal = workerInformation.get_best_proposal();
      MPIHandler::send_data_to_master(proposal);
      if (proposal != NULL) {
        delete proposal;
        proposal = NULL;
      }
      // info instance deleted in read_new_work WorkerInformation function
    }
  }

  // Finish of MPI
  MPI_Finalize();
  return 0;
}
