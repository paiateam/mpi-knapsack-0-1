#include "TransferClasses.hpp"

using namespace std;

/* ToWorkInfo */
ToWorkInfo::ToWorkInfo() {
  assignments = nullptr;
  indexs = nullptr;
  probabilities = nullptr;
}

ToWorkInfo::~ToWorkInfo() {
  if (assignments != nullptr) {
    delete[] assignments;
    assignments = nullptr;
  }
  if (indexs != nullptr) {
    delete[] indexs;
    indexs = nullptr;
  }
  if (probabilities != nullptr) {
    delete[] probabilities;
    probabilities = nullptr;
  }
}

/* Proposal */

Proposal::Proposal() {
  best_solution = nullptr;
  indexs = nullptr;
  probabilities = nullptr;
}

Proposal::~Proposal() {
  if (best_solution != nullptr) {
    delete[] best_solution;
    best_solution = nullptr;
  }
  if (indexs != nullptr) {
    delete[] indexs;
    indexs = nullptr;
  }
  if (probabilities != nullptr) {
    delete[] probabilities;
    probabilities = nullptr;
  }
}
