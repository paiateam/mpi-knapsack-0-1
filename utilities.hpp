#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <vector>

// Public functions
void print_vector(const std::vector<int> &v);
void sort_vectors(std::vector<int> &v, std::vector<int> &v_aux1);
void array_init_int(int *array, int size, int value);
void array_init_float(float *array, int size, float value);
int * array_copy(int size, int * arr);
float * array_copy(int size, float * arr);
void print_array(int * array, int size);
void print_array(float * array, int size);

// Internal functions
void _quick_sort(std::vector<int> &v, std::vector<int> &v_aux1, int l, int h);
int partition (std::vector<int> &v, std::vector<int> &v_aux1, int l, int h);
void swap ( int i, int j, std::vector<int> &v);

#endif
