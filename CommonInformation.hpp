#ifndef COMMONINFORMATION_HPP
#define COMMONINFORMATION_HPP

#include <string>
#include <vector>

class CommonInformation {
  int totalItems;
  int knapsackCapacity;
  int globalCost;
  int totalWeight;
  std::vector<int> values;
  std::vector<int> weights;
  int rank;
public:
  void set_rank(int rank);
  void read_file (std::string path);
  void print_info();
  int get_total_items();
  int get_knapsack_capacity();
  int get_total_weight();
  int get_total_value();
  int get_num_zones();
  int get_weight_of_index(int index);
  int get_value_of_index(int index);
private:
  void print(std::string s);
  void check_totals(int weight_sum, int cost_sum);
};

#endif
