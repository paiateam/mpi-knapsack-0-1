#ifndef TRANSFERCLASSES_HPP
#define TRANSFERCLASSES_HPP

class ToWorkInfo {
public:
  int * assignments;
  int num_items;
  int current_value;
  int current_weight;
  int current_knapsack;
  int max_iterations;
  int max_tries;
  int min_weight;
  int * indexs; // Indexs to split assignations vector in zones
  float * probabilities; // Array of probabilities that has each zone
  int num_probabilities; // Same num of probabilities than indexs
  ToWorkInfo();
  ~ToWorkInfo();
};

class Proposal {
public:
  int best_value;
  int best_weight;
  int * best_solution;
  int num_items;
  int * indexs; // Indexs to split assignations vector in zones
  float * probabilities; // Array of probabilities that has each zone
  int num_probabilities;
  Proposal();
  ~Proposal();
};

#endif
