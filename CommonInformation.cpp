#include <iostream>
#include <fstream>
#include <regex>
#include <cmath>

#include "CommonInformation.hpp"
#include "utilities.hpp"

using namespace std;

void CommonInformation::set_rank(int rank) {
  CommonInformation::rank = rank;
}

void CommonInformation::read_file(string path) {
  // Variables to check file correctness
  int weight_sum = 0;
  int cost_sum = 0;

  // Initialization of totalWeight and globalCost
  totalWeight = 0;
  globalCost = 0;

  // Regex that can appear
  regex header_regex("(\\d+)\\s+(\\d+)"); // 1st: num items. 2nd: size of knapsak
  regex objects_regex("(\\d+)\\s*,\\s*(\\d+)"); // 1st value. 2nd weight
  regex global_cost_regex("GLOBAL\\sCOST:\\s*(\\d+)");
  regex total_weight_regex("TOTAL\\sLOAD:\\s*(\\d+)");

  smatch m; // match structure

  string line;
  ifstream inputFile(path.c_str());
  if (inputFile.is_open()) {
    int num_line = 1;
    while (getline(inputFile, line)) {
      if (regex_match(line, m, header_regex)) {
        // Header match
        totalItems = stoi(m[1]); // m[1] contains the number of items
        knapsackCapacity = stoi(m[2]); // m[2] contains the knapsack capacity
      } else if (regex_match(line, m, objects_regex)) {
        // Object match
        int value = stoi(m[1]);
        int weight = stoi(m[2]);
        if (weight == 0) {
          CommonInformation::print("ERROR: Line " + to_string(num_line) +
                                   ". Weight equals to 0 found. Check the instance file.");
          inputFile.close();
          exit(EXIT_FAILURE);
        }
        cost_sum += value; // Sum value
        weight_sum += weight; // Sum weight
        values.push_back(value); // m[1] contains value of the object
        weights.push_back(weight); // m[2] contains weight of the object
      } else if (regex_match(line, m, global_cost_regex)) {
        // Global cost match
        globalCost = stoi(m[1]); // m[1] contains the global cost
      } else if (regex_match(line, m, total_weight_regex)) {
        // Total weight match
        totalWeight = stoi(m[1]); // m[1] contains the total weight
      } else {
        // Unknown line
        CommonInformation::print("WARNING: Line " + to_string(num_line) +
                                 ". Unknown line: " + line);
      }
      num_line++;
    }
    inputFile.close();
    sort_vectors(weights, values);
    // Check globalCost and totalWeight values
    CommonInformation::check_totals(weight_sum, cost_sum);
  } else {
    CommonInformation::print("ERROR: Unable to read the file");
    exit(EXIT_FAILURE);
  }
}

void CommonInformation::print_info() {
  cout << "Total Items: " << totalItems << endl;
  cout << "Knapsack Capacity: " << knapsackCapacity << endl;
  cout << "Global Cost: " << globalCost << endl;
  cout << "Total weight: " << totalWeight << endl;
  cout << "Solution between " << to_string((int) ceil(totalWeight/knapsackCapacity));
  cout << " and " << to_string(totalItems) << " knapsacks" << endl;
}

int CommonInformation::get_total_items() {
  return totalItems;
}

int CommonInformation::get_knapsack_capacity() {
  return knapsackCapacity;
}

int CommonInformation::get_total_weight() {
  return totalWeight;
}

int CommonInformation::get_total_value() {
  return globalCost;
}

int CommonInformation::get_num_zones() {
  // In some point of the implementation it can be dynamic
  return 10;
}

int CommonInformation::get_weight_of_index(int index) {
  if (index < 0 || index >= totalItems) {
    return -1;
  }
  return weights[index];
}

int CommonInformation::get_value_of_index(int index) {
  if (index < 0 || index >= totalItems) {
    return -1;
  }
  return values[index];
}

/* PRIVATE METHODS */

void CommonInformation::print(string s) {
  if (rank == 0) {
    cout << s << endl;
  }
}

void CommonInformation::check_totals(int weight_sum, int cost_sum) {
  // Global Cost or Total Load lines not found
  if (totalWeight == 0) {
    CommonInformation::print("Total Load line not found. Setting total weight to "
                              + to_string(weight_sum));
    totalWeight = weight_sum;
  }
  if (globalCost == 0) {
    CommonInformation::print("Global Cost line not found. Setting global cost to "
                              + to_string(cost_sum));
    globalCost = cost_sum;
  }
  // Wrong sum of Total Load or Global Cost
  if (totalWeight != weight_sum) {
    CommonInformation::print("WARNING: Total Load value " + to_string(totalWeight)
                              + " does not match with the real weight " + to_string(weight_sum)
                              + ". Setting total weight to " + to_string(weight_sum));
    totalWeight = weight_sum;
  }
  if (globalCost != cost_sum) {
    CommonInformation::print("WARNING: Global Cost value " + to_string(globalCost)
                              + " does not match with the real cost " + to_string(cost_sum)
                              + ". Setting global cost to " + to_string(cost_sum));
    globalCost = cost_sum;
  }
}
