#include "mpi.h"
#include <vector>

#include "MPIHandler.hpp"
#include "TransferClasses.hpp"

using namespace std;

int MPIHandler::to_work_struct_size = 0;
int MPIHandler::proposal_struct_size = 0;

void MPIHandler::init(int num_items, int num_probabilities) {
  MPIHandler::to_work_struct_size = MPIHandler::calculate_to_work_struct_size(num_items,
                                                                              num_probabilities);
  MPIHandler::proposal_struct_size = MPIHandler::calculate_proposal_struct_size(num_items,
                                                                                num_probabilities);
}

void MPIHandler::pack_to_work_info(ToWorkInfo &info, char *buffer) {
  // PRECONDITION: MPIHandler::initialize(num_items, num_probabilities)
  int pos = 0;
  // num_items
  MPI_Pack(&info.num_items, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // current_value
  MPI_Pack(&info.current_value, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // current_weight
  MPI_Pack(&info.current_weight, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // current_knapsack
  MPI_Pack(&info.current_knapsack, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // max_iterations
  MPI_Pack(&info.max_iterations, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // max_tries
  MPI_Pack(&info.max_tries, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // min_weight
  MPI_Pack(&info.min_weight, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // num_probabilities
  MPI_Pack(&info.num_probabilities, 1, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // assignments
  MPI_Pack(info.assignments, info.num_items, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // indexs
  MPI_Pack(info.indexs, info.num_probabilities, MPI_INT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
  // probabilities
  MPI_Pack(info.probabilities, info.num_probabilities, MPI_FLOAT, buffer,
           MPIHandler::to_work_struct_size, &pos, MPI_COMM_WORLD);
}

void MPIHandler::pack_proposal_info(Proposal &info, char *buffer) {
  // PRECONDITION: MPIHandler::initialize(num_items, num_probabilities)
  int position = 0;
  // best_value
  MPI_Pack(&info.best_value, 1, MPI_INT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
  // best_weight
  MPI_Pack(&info.best_weight, 1, MPI_INT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
  // num_items
  MPI_Pack(&info.num_items, 1, MPI_INT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
  // num_probabilities
  MPI_Pack(&info.num_probabilities, 1, MPI_INT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
  // best_solution
  MPI_Pack(info.best_solution, info.num_items, MPI_INT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
  // indexs
  MPI_Pack(info.indexs, info.num_probabilities, MPI_INT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
  // probabilities
  MPI_Pack(info.probabilities, info.num_probabilities, MPI_FLOAT, buffer,
           MPIHandler::proposal_struct_size, &position, MPI_COMM_WORLD);
}

ToWorkInfo * MPIHandler::unpack_to_work_info(char *buffer) {
  // PRECONDITION: MPIHandler::initialize(num_items, num_probabilities)
  ToWorkInfo * info = new ToWorkInfo();
  int position = 0;
  // Unpack num_items
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->num_items, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack current_value
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->current_value, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack current_weight
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->current_weight, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack current_knapsack
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->current_knapsack, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack max_iterations
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->max_iterations, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack max_tries
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->max_tries, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack min_weight
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->min_weight, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack num_probabilities
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, &info->num_probabilities, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack assignments
  info->assignments = new int[info->num_items];
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, info->assignments, info->num_items, MPI_INT, MPI_COMM_WORLD);
  // Unpack indexs
  info->indexs = new int[info->num_probabilities];
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, info->indexs, info->num_probabilities, MPI_INT, MPI_COMM_WORLD);
  // Unpack probabilities
  info->probabilities = new float[info->num_probabilities];
  MPI_Unpack(buffer, MPIHandler::to_work_struct_size,
             &position, info->probabilities, info->num_probabilities, MPI_FLOAT, MPI_COMM_WORLD);
  // Return struct
  return info;
}

Proposal * MPIHandler::unpack_proposal_info(char *buffer) {
  // PRECONDITION: MPIHandler::initialize(num_items, num_probabilities)
  Proposal * info = new Proposal();
  int position = 0;
  // Unpack best_value
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, &info->best_value, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack best_weight
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, &info->best_weight, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack num_items
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, &info->num_items, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack num_probabilities
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, &info->num_probabilities, 1, MPI_INT, MPI_COMM_WORLD);
  // Unpack best_solution
  info->best_solution = new int[info->num_items];
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, info->best_solution, info->num_items, MPI_INT, MPI_COMM_WORLD);
  // Unpack indexs
  info->indexs = new int[info->num_probabilities];
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, info->indexs, info->num_probabilities, MPI_INT, MPI_COMM_WORLD);
  // Unpack probabilities
  info->probabilities = new float[info->num_probabilities];
  MPI_Unpack(buffer, MPIHandler::proposal_struct_size,
             &position, info->probabilities, info->num_probabilities, MPI_FLOAT, MPI_COMM_WORLD);
  // Return struct
  return info;
}

int MPIHandler::get_to_work_struct_size() {
  return MPIHandler::to_work_struct_size;
}

int MPIHandler::get_proposal_struct_size() {
  return MPIHandler::proposal_struct_size;
}

void MPIHandler::scatter_to_work_data(vector<ToWorkInfo *> data) {
  int tag = 0;
  char * buffer;
  for (int i = 0; i < data.size(); i++) {
    buffer = new char[MPIHandler::to_work_struct_size];
    MPIHandler::pack_to_work_info(*data[i], buffer);
    MPI_Send(buffer, MPIHandler::to_work_struct_size,
             MPI_PACKED, i+1, tag, MPI_COMM_WORLD);
    delete[] buffer;
  }
}

ToWorkInfo * MPIHandler::receive_master_data() {
  int root = 0;
  MPI_Status status;
  char * buffer = new char[MPIHandler::to_work_struct_size];
  MPI_Recv(buffer, MPIHandler::to_work_struct_size,
           MPI_PACKED, root, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  // TODO: Check MPI_Status
  ToWorkInfo * info = MPIHandler::unpack_to_work_info(buffer);
  delete[] buffer;
  return info;
}

void MPIHandler::send_data_to_master(Proposal * data) {
  int root = 0;
  int tag = 0;
  char * buffer = new char[MPIHandler::proposal_struct_size];
  MPIHandler::pack_proposal_info(*data, buffer);
  MPI_Send(buffer, MPIHandler::proposal_struct_size,
           MPI_PACKED, root, tag, MPI_COMM_WORLD);
  delete[] buffer;
}

void MPIHandler::gather_data_from_workers(vector<Proposal *> &proposals, int size) {
  char * buffer;
  MPI_Status status;
  for (int i = 0; i < proposals.size(); i++) {
    if (proposals[i] != nullptr) {
      delete proposals[i];
      proposals[i] = nullptr;
    }
  }
  proposals.clear();

  for (int i = 0; i < size; i++) {
    buffer = new char[MPIHandler::proposal_struct_size];
    MPI_Recv(buffer, MPIHandler::proposal_struct_size,
             MPI_PACKED, i+1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    // TODO: Check MPI_Status
    proposals.push_back(MPIHandler::unpack_proposal_info(buffer));
    delete[] buffer;
  }
}

int MPIHandler::calculate_to_work_struct_size(int num_items, int num_probabilities) {
  int result = 0;
  int size = 0;

  // assignments
  MPI_Pack_size(num_items, MPI_INT, MPI_COMM_WORLD, &size);
  result += size;
  // num_items, current_value, current_weight, current_knapsack, max_iterations,
  // max_tries, min_weight, num_probabilities
  MPI_Pack_size(8, MPI_INT, MPI_COMM_WORLD, &size);
  result += size;
  // indexs
  MPI_Pack_size(num_probabilities, MPI_INT, MPI_COMM_WORLD, &size);
  result += size;
  // probabilities
  MPI_Pack_size(num_probabilities, MPI_FLOAT, MPI_COMM_WORLD, &size);
  result += size;

  return result;
}

int MPIHandler::calculate_proposal_struct_size(int num_items, int num_probabilities) {
  int result = 0;
  int size = 0;

  // best_solution
  MPI_Pack_size(num_items, MPI_INT, MPI_COMM_WORLD, &size);
  result += size;
  // best_value, best_weight, num_items, num_probabilities
  MPI_Pack_size(4, MPI_INT, MPI_COMM_WORLD, &size);
  result += size;
  // indexs
  MPI_Pack_size(num_probabilities, MPI_INT, MPI_COMM_WORLD, &size);
  result += size;
  // probabilities
  MPI_Pack_size(num_probabilities, MPI_FLOAT, MPI_COMM_WORLD, &size);
  result += size;

  return result;
}

MPIHandler::MPIHandler() { }
