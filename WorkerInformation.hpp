#ifndef WORKERINFORMATION_HPP
#define WORKERINFORMATION_HPP

#include <vector>
#include "CommonInformation.hpp"
#include "TransferClasses.hpp"

// Worker information class definition
class WorkerInformation {
  // Problem variables
  CommonInformation commonInformation;
  // Best values found
  int best_value;
  int best_weight;
  int current_weight;
  int current_value;
  int * best_solution; // Array with the best solution found
  int * current_assingments;
  float * current_probabilities;
  float * best_probabilities;
  int total_available_zones;
  int work_available_zones;
  // Search variables
  ToWorkInfo * to_work_info;
public:
  WorkerInformation(CommonInformation c, int rank);
  int get_max_tries();
  int get_max_iterations();
  int get_current_weight();
  int get_next_item();
  bool add_item_attempt(int index);
  void read_new_work(ToWorkInfo * info);
  void search_init();
  void search_finish();
  Proposal * get_best_proposal();
private:
  int get_zone_item();
  int pick_item_from_zone(int zone);
  float percentage_to_redistribute(int zone, float reduce_factor);
  void update_probabilities(float percentage, int excluded_zone, bool is_deleting);
  void calculate_available_zones();
};

#endif
