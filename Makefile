STD=-std=c++11

release: *.cpp *.hpp
	mpic++ -O2 $(STD) *.cpp *.hpp -o main

dynamic: *.cpp *.hpp
	mpic++ $(STD) *.cpp *.hpp -o main
