#include <stdlib.h>
#include <time.h>

#include "WorkerInformation.hpp"
#include "CommonInformation.hpp"
#include "TransferClasses.hpp"
#include "utilities.hpp"


using namespace std;

WorkerInformation::WorkerInformation(CommonInformation c, int rank) {
  commonInformation = c;
  srand(time(nullptr) * rank);
  best_solution = nullptr;
  current_assingments = nullptr;
  current_probabilities = nullptr;
  best_probabilities = nullptr;
}

int WorkerInformation::get_max_tries(){
  return to_work_info->max_tries;
}

int WorkerInformation::get_current_weight(){
  return current_weight;
}

int WorkerInformation::get_max_iterations(){
  return to_work_info->max_iterations;
}

bool WorkerInformation::add_item_attempt(int index){
  if((current_weight + commonInformation.get_weight_of_index(index))
  <= commonInformation.get_knapsack_capacity()){
    current_assingments[index] = to_work_info->current_knapsack;
    current_weight += commonInformation.get_weight_of_index(index);
    current_value += commonInformation.get_value_of_index(index);
  }
  return (commonInformation.get_knapsack_capacity() - current_weight)
    >= to_work_info->min_weight;

}

void WorkerInformation::read_new_work(ToWorkInfo * info){
  to_work_info = info;
  best_weight = to_work_info->current_weight;
  best_value = to_work_info->current_value;
  if (current_probabilities != nullptr) {
    delete[] current_probabilities;
    current_probabilities = nullptr;
  }
  current_probabilities = array_copy(to_work_info->num_probabilities, to_work_info->probabilities);
  calculate_available_zones();
  total_available_zones = work_available_zones;
  if (best_solution != nullptr) {
    delete[] best_solution;
    best_solution = nullptr;
  }
  best_solution = array_copy(to_work_info->num_items, to_work_info->assignments);
}

void WorkerInformation::search_init(){
  total_available_zones = work_available_zones;
  current_weight = to_work_info->current_weight;
  current_value = to_work_info->current_value;
  if (current_assingments != nullptr) {
    delete[] current_assingments;
    current_assingments = nullptr;
  }
  current_assingments = array_copy(to_work_info->num_items, to_work_info->assignments);
}

void WorkerInformation::search_finish(){
  if(current_weight > best_weight){
    best_weight = current_weight;
    best_value = current_value;
    if (best_solution != nullptr) {
      delete[] best_solution;
      best_solution = nullptr;
    }
    best_solution = array_copy(to_work_info->num_items, current_assingments);
    if (best_probabilities != nullptr) {
      delete[] best_probabilities;
      best_probabilities = nullptr;
    }
    best_probabilities = array_copy(to_work_info->num_probabilities, current_probabilities);
  }
}

Proposal * WorkerInformation::get_best_proposal(){
  Proposal * proposal = new Proposal();
  proposal->best_value = best_value;
  proposal->best_weight = best_weight;
  proposal->num_items = to_work_info->num_items;
  proposal->best_solution = best_solution;//array_copy(to_work_info->num_items, best_solution);
  proposal->indexs = to_work_info->indexs;//array_copy(to_work_info->num_probabilities, to_work_info->indexs); //TODO: Recalculate index zones
  proposal->probabilities = to_work_info->probabilities;//array_copy(to_work_info->num_probabilities, to_work_info->probabilities);
  proposal->num_probabilities = to_work_info->num_probabilities;
  return proposal;
}

int WorkerInformation::get_next_item(){
  int zone;
  int item = -1;
  zone = get_zone_item();
  item = pick_item_from_zone(zone);
  return item;
}

int WorkerInformation::get_zone_item(){
  float rnd_zone = rand() % 100 + 1;
  int i;
  for(i = 0; i < to_work_info->num_probabilities; i++){
    if(rnd_zone <= current_probabilities[i])
      return i;
  }
  return i - 1;
}

int WorkerInformation::pick_item_from_zone(int zone){
  int mod = to_work_info->indexs[zone];
  int zone_start = 0;
  float percentage;
  if(zone != 0){
    zone_start = to_work_info->indexs[zone-1];
    mod -= zone_start;
  }
  int index = rand() % mod;
  int i = (index + 1) % mod;

  while(current_assingments[zone_start+i] != 0){
    if(i == index){
      percentage = percentage_to_redistribute(zone, 1);
      total_available_zones --;
      update_probabilities(percentage, zone, true);
      return -1;
    }
    i++;
    i %= mod;
  }

  percentage = percentage_to_redistribute(zone, 0.5f);
  update_probabilities(percentage, zone, false);
  return zone_start + i;
}

float WorkerInformation::percentage_to_redistribute(int zone, float reduce_factor){
  float currentPercentage;
  if(zone == 0){
    currentPercentage = current_probabilities[0];
  }
  else{
    currentPercentage = current_probabilities[zone] - current_probabilities[zone-1];
  }

  return currentPercentage * reduce_factor;

}

void WorkerInformation::update_probabilities(float percentage, int excluded_zone, bool is_deleting){
  float percPart;
  float previous = 0;
  int totalZones = total_available_zones - 1;
  percPart = percentage/totalZones;

  bool nextNull = false;
  bool currentNull = false;

  for(int i = 0; i < excluded_zone; i++){

    currentNull = nextNull;

    if(current_probabilities[i] == current_probabilities[i+1])
      nextNull = true;
    else{
      nextNull = false;
    }

    if(currentNull){
      current_probabilities[i] = current_probabilities [i-1];
      continue;
    }
    if(current_probabilities[i] == 0){
      continue;
    }

    previous += percPart;
    current_probabilities[i] += previous;
  }

  previous = 0;

  nextNull = false;

  for(int f = to_work_info->num_probabilities-2; f >= excluded_zone; f--){
    currentNull = nextNull;
    if(f > 0 && current_probabilities[f-1] == current_probabilities[f]){
      nextNull = true;
    }
    else{
      nextNull = false;
    }

    previous += percPart;
    current_probabilities[f] -= previous;

    if(currentNull){
      current_probabilities[f+1] = current_probabilities[f];
    }
  }

  if(is_deleting){
    if(excluded_zone == 0){
      current_probabilities[0] = 0;
    }
    else{
      current_probabilities[excluded_zone] = current_probabilities[excluded_zone-1];
    }
  }

}

void WorkerInformation::calculate_available_zones(){
  work_available_zones = to_work_info->num_probabilities;
  int previuous_value = 0;
  for(int i = 0; i < to_work_info->num_probabilities; i++){
    if(to_work_info->probabilities[i] == previuous_value){
      work_available_zones--;
    }
    previuous_value = to_work_info->probabilities[i];
  }
}
